<?php

    include_once '../../Config/connectionDb.php';

    if(intval($id) == 0){                                   // If id = 0 or empty return this
        http_response_code(503);
        echo json_encode(array("message" => "Please choose a topic (ex: 'https://example.php/2')"));  
    }

    else{
        $stmt = $pdo->prepare($sql);
        $fetchstmt = fetch($stmt);
        if($fetchstmt != false){    // Verifys in topic if object exist 
            $sql_post = "DELETE FROM post WHERE post.topic_id = $id";
            $sql = "DELETE FROM topic WHERE topic.id = $id";
            $stmt_post = $pdo->prepare($sql_post);      
            $stmt = $pdo->prepare($sql);
            $stmt_post->execute();                       //Delete the post of the topic that will be deleted
            $stmt->execute();                           // Delete topic
            http_response_code(200);
            echo json_encode(["message" => "Topic remove"]);
        }

        else{
            http_response_code(503);                                //If it doesn't exists return this
            echo json_encode(["message" => "This topic doesn't exist or can't be delete"]);    
        }
    }
<?php
include_once '../../Config/header.php';
header("Access-Control-Allow-Methods: GET, POST, PUT"); // authorizes GET, POST and PUT
include_once '../../Config/method.php';


$title = secur_data($data->title);
$category_id = secur_data($data->category_id);            // Recups data that we post and secures it
$user_id = secur_data($data->user_id);
$topic_id = secur_data($data->id);

if($_SERVER['REQUEST_METHOD'] == 'GET'){            // Does if method is get

    include_once '../../Config/connectionDb.php';

    $sql = "SELECT t.id,t.title,t.category_id,category.label,t.user_id,user.email AS user FROM topic AS t  
    LEFT JOIN category ON t.category_id = category.id 
    LEFT JOIN user ON t.user_id = user.id";

    $stmt = $pdo->prepare($sql);
    $fetchstmt = fetch($stmt);                  // Recups all field from topic and puts in associative array
    $return["count"] = count($fetchstmt);       // Counts how many topic object in this array
    foreach($fetchstmt as $row ){
        extract($row);
        if($user != null){                              //Verifys if user exist , if not return "The user has been deleted"
            $topic = [
                "id" => $id,
                "title" => $title,
                "category_id" => $category_id,
                "label" => $label,
                "user_id" => $user_id,
                "user" => $user
            ];
            
            $return["results"]["Topics"][] = $topic;}     //return this in Topic array
            else{
                $topic = [
                    "id" => $id,
                    "title" => $title,
                    "category_id" => $category_id,
                    "label" => $label,
                    "user_id" => $user_id,
                    "user" => "The user has been deleted"
                ];
                $return["results"]["Topics"][] = $topic;
                
            }
        }
    http_response_code(200);
    echo json_encode($return);                  // Display in json 
}
elseif($_SERVER['REQUEST_METHOD'] == 'POST'){           // Does if method is "post"
    include_once('create.php');
}
elseif($_SERVER['REQUEST_METHOD'] == 'PUT'){        // Does if method is "put"
    include_once('update_put.php');
}
else{
    http_response_code(405);
    echo json_encode(["message" => "Method is not allowed"]);       // Return this if no methods work 
}
    <?php
    include_once '../../Config/header.php';
    header("Access-Control-Allow-Methods: GET, DELETE, PATCH"); //Authorizes GET, DELETE and PATCH
    include_once '../../Config/method.php';

    $sql = "SELECT t.id,t.title,t.category_id,category.label,t.user_id,user.email AS user FROM topic AS t  
    LEFT JOIN category ON t.category_id = category.id 
    LEFT JOIN user ON t.user_id = user.id
    WHERE t.id = $id";                                      // A sql request , recups topic by id in url

    if($_SERVER['REQUEST_METHOD'] == 'GET'){                // Does if method is "get"

        include_once '../../Config/connectionDb.php';

        if(intval($id) == 0){                                   // If id = 0 or empty return this
            http_response_code(404);    
            echo json_encode(array("message" => "Please choose a topic (ex: 'https://example.php?id=2')"));
        }
        else{
            $stmt = $pdo->prepare($sql);
            $fetchstmt = fetch($stmt);
            if($fetchstmt != null){                     // Verifys in category if object exist
                foreach($fetchstmt as $row ){
                extract($row);
                if($user != null){                           //verifys if user exist , if not return "The user has been deleted"
                    $topic = [
                        "id" => $id,
                        "title" => $title,
                        "category_id" => $category_id,
                        "label" => $label,
                        "user_id" => $user_id,
                        "user" => $user
                    ];
                    
                    $return["results"]["Topic"][] = $topic;}     //return this in Topic array
                    else{
                        $topic = [
                            "id" => $id,
                            "title" => $title,
                            "category_id" => $category_id,
                            "label" => $label,
                            "user_id" => $user_id,
                            "user" => "The user has been deleted"
                        ];
                        $return["results"]["Topic"][] = $topic;
                        
                    }
                }
                http_response_code(200);
                echo json_encode($return);
            }
            else{
                http_response_code(404);
            
                echo json_encode(array("message" => "Topic doesn't exist"));        // If this topic doesn't exist return this
            }
        }
    
    }
    elseif($_SERVER['REQUEST_METHOD'] == 'DELETE'){                 // Does if method is "delete"
        include_once('delete.php');
    
    }
    elseif($_SERVER['REQUEST_METHOD'] == 'PATCH'){              // Does if method is "patch"
        include_once('update_patch.php');
    
    }
    else{
        http_response_code(405);
        echo json_encode(["message" => "Method is not allowed"]);       // Return this if no methods work 
    }
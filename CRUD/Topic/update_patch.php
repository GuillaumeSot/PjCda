<?php
    include_once '../../Config/connectionDb.php';

    if(intval($id) == 0){                                   // If id = 0 or empty return this
        http_response_code(503);
        echo json_encode(array("message" => "Please choose a topic (ex: 'https://example.php/2')"));  
    }
else{

        $title = secur_data($data->title);
        $category_id = secur_data($data->category_id);          // Recups data that we post and secures it
        $user_id = secur_data($data->user_id);

        $stmt = $pdo->prepare($sql);
        $fetchstmt = fetch($stmt);      // Recups field of topic by id and put it on associative array
        $title = ifempty($title ,$fetchstmt[0]["title"]);
        $category_id = ifempty($category_id ,$fetchstmt[0]["category_id"]); // If we send data in the field, data = new one,else we keep current data
        $user_id = ifempty($user_id ,$fetchstmt[0]["user_id"]);    

        if ($data->title == null && $data->category_id == null && $data->user_id == null){ //Verifys that at least one field has received data, if no field has received data return this
            echo json_encode(["message" => "Nothing was modified"]);
            http_response_code(503);
        }
        else{
            $request = $pdo->prepare("SELECT * FROM category 
            WHERE category.id = '".$category_id."'");
            $stmtfetch = fetch($request);    

           

                if(!empty($title) && isset($title) &&
                !empty($category_id) && isset($category_id) &&
                !empty($user_id) && isset($user_id) && $fetchstmt != null){ // Verifys that data send are not null and not empty
                    if($stmtfetch == null){                                     //Verifys if category exist                                            
                        echo json_encode(["message" => "Category doesn't exist"]);
                        http_response_code(403);
                    }
                    else{

                    $stmt = $pdo->prepare("UPDATE topic SET title = :titre ,category_id = :cate, user_id = :ui WHERE id = $id");
                    $stmt->bindParam(':titre', $title);
                    $stmt->bindParam(':cate', $category_id);
                    $stmt->bindParam(':ui', $user_id);             //Update this
                    $stmt->execute();

                    http_response_code(200);
                    echo json_encode(["message" => "A topic has been edit"]);
                }
            }
                else{
                    http_response_code(503);
                    echo json_encode(["message" => "modification does not work"]);            // If one data are null or empty return this 
                }
         }
    }

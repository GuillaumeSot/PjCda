<?php

    include_once '../../Config/connectionDb.php';

    // $title = secur_data($_POST['title']);
    // $category_id = secur_data($_POST['category_id']);            By url
    // $user_id = secur_data($_POST['user_id']);

    $request = $pdo->prepare("SELECT * FROM category 
    WHERE category.id = '".$category_id."'");
    $stmtfetch = fetch($request);    
    
    if($stmtfetch == null){                                     //Verifys if category exist                                            
        echo json_encode(["message" => "Category doesn't exist"]);
        http_response_code(403);
    }
    else{

        if(!empty($title) && isset($title) &&                   // Verifys that data send are not null and not empty
        !empty($category_id) && isset($category_id) &&
        !empty($user_id) && isset($user_id) ){

            $stmt = $pdo->prepare("INSERT INTO topic (title,category_id,user_id) VALUES (:titre, :cate, :ui);");
            $stmt->bindParam(':titre', $title);
            $stmt->bindParam(':cate', $category_id);                    //Add data at category
            $stmt->bindParam(':ui', $user_id);
            $stmt->execute();

            http_response_code(201);
            echo json_encode(["message" => "A topic has been added"]);

        }

        else{
                http_response_code(503);
                echo json_encode(["message" => "Nothing was add"]);     // If one data send are empty or null return this
            }
        }

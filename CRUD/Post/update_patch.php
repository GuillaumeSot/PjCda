<?php
    include_once '../../Config/connectionDb.php';

    if(intval($id) == 0){                                   // If id = 0 or empty return this
        http_response_code(503);
        echo json_encode(array("message" => "Please choose a post (ex: 'https://example.php/2')"));  
    }
else{

        $content = secur_data($data->content);
        $topic_id = secur_data($data->topic_id);                          // Recups data that we post and secures it
        $user_id = secur_data($data->user_id);

        $stmt = $pdo->prepare($sql);
        $fetchstmt = fetch($stmt);                              // Recups field of post by id and put it on associative array

        $content = ifempty($content ,$fetchstmt[0]["content"]);
        $topic_id = ifempty($topic_id ,$fetchstmt[0]["topic_id"]);            // If we send data in the field, data = new one,else we keep current data
        $user_id = ifempty($user_id ,$fetchstmt[0]["user_id"]);

        if ($data->content == null && $data->topic_id == null && $data->user_id == null){
            echo json_encode(["message" => "Nothing was modified"]);                  //Verifys that at least one field has received data, if no field has received data return this
            http_response_code(503);
        }

        else{
            $request = $pdo->prepare("SELECT * FROM topic 
            WHERE topic.id = '".$topic_id."'");
            $stmtfetch = fetch($request);    
            


                if(!empty($content) && isset($content) &&
                !empty($topic_id) && isset($topic_id) &&                        // Verifys that data send are not null and not empty
                !empty($user_id) && isset($user_id) && $fetchstmt != null){
                    if($stmtfetch == null){                                     //Verifys if topic exist                                            
                        echo json_encode(["message" => "Topic doesn't exist"]);
                        http_response_code(403);
                    }
                    else{        
                        $stmt = $pdo->prepare("UPDATE post SET content = :cont,topic_id = :topic,user_id = :user WHERE id = $id");
                        $stmt->bindParam(':cont', $content);
                        $stmt->bindParam(':topic', $topic_id);
                        $stmt->bindParam(':user', $user_id);                        //Update this
                        $stmt->execute();

                        http_response_code(200);
                        echo json_encode(["message" => "A post has been edit"]);
        }
    }
        else{
            http_response_code(503);
            echo json_encode(["message" => "modification does not work"]);        // If one data are null or empty return this 
            }
        
    }
    
}
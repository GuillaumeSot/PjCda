<?php
include_once '../../Config/header.php';
header("Access-Control-Allow-Methods: GET, POST, PUT"); // authorizes GET, POST and PUT
include_once '../../Config/method.php';

$post_date =  date("Y-m-d H:i:s");
$content = secur_data($data->content);
$topic_id = secur_data($data->topic_id);                      // Recups data that we post and secures it
$user_id = secur_data($data->user_id);
$post_id = secur_data($data->id);

if($_SERVER['REQUEST_METHOD'] == 'GET'){                        // Does if method is get

include_once '../../Config/connectionDb.php';

$sql = "SELECT p.id,p.post_date,p.content,p.topic_id,topic.title AS topic_title,p.user_id,user.email AS user,category.label AS category_label_topic FROM post AS p
    LEFT JOIN topic ON p.topic_id = topic.id
    LEFT JOIN user ON p.user_id = user.id 
    LEFT JOIN category ON topic.category_id = category.id";

    $stmt = $pdo->prepare($sql);
    $fetchstmt = fetch($stmt);                          // Recups all field from post and puts in associative array
    $return["count"] = count($fetchstmt);               // Counts how many post object in this array

    foreach($fetchstmt as $row ){
                
        extract($row);
        if($user != null){                  //Verifys if user exist , if not return "The user has been deleted"
            $post = [
                "id" => $id,
                "post_date" => $post_date,
                "content" => $content,
                "topic_id" => $topic_id,
                "topic_title" => $topic_title,
                "user_id" => $user_id,
                "user" => $user,
                "category_label_topic" => $category_label_topic
            ];
            
            $return["results"]["Posts"][] = $post;                  //return this in Post array
            }
        else{
            $post = [
                "id" => $id,
                "post_date" => $post_date,
                "content" => $content,
                "topic_id" => $topic_id,
                "topic_title" => $topic_title,
                "user_id" => $user_id,
                "user" => "The user has been deleted",
                "category_label_topic" => $category_label_topic
            ];
            
            $return["results"]["Posts"][] = $post;        

            }    
        }
    http_response_code(200);                                        
    echo json_encode($return);                                                      // Display in json 
}

elseif($_SERVER['REQUEST_METHOD'] == 'POST'){                               // Does if method is "post"
    include_once('create.php');
}

elseif($_SERVER['REQUEST_METHOD'] == 'PUT'){                                // Does if method is "put"
    include_once('update_put.php');
}

else{
    http_response_code(405);
    echo json_encode(["message" => "Method is not allowed"]);                   // Return this if no methods work 
}
<?php

    include_once '../../Config/connectionDb.php';

    // $post_date = secur_data($_POST['post_date']);            By url
    // $content = secur_data($_POST['content']);
    // $topic_id = secur_data($_POST['topic_id']);
    // $user_id = secur_data($_POST['user_id']);

    
    $post_date =  date("Y-m-d H:i:s");    
                  //$post_date = current date
    $request = $pdo->prepare("SELECT * FROM topic 
    WHERE topic.id = '".$topic_id."'");
    $stmtfetch = fetch($request);    
    
    if($stmtfetch == null){                                     //Verifys if topic exist                                            
        echo json_encode(["message" => "Topic doesn't exist"]);
        http_response_code(403);
    }
    else{    

        if(!empty($content) && isset($content) &&
        !empty($topic_id) && isset($topic_id) &&                   // Verifys that data send are not null and not empty
        !empty($user_id) && isset($user_id)){

            $stmt = $pdo->prepare("INSERT INTO post (post_date,content,topic_id,user_id) VALUES (:date, :cont, :topic, :user);");
            $stmt->bindParam(':date', $post_date);
            $stmt->bindParam(':cont', $content);
            $stmt->bindParam(':topic', $topic_id);                      //Add data at post
            $stmt->bindParam(':user', $user_id);
            $stmt->execute();

            http_response_code(201);
            echo json_encode(["message" => "A post has been posted"]);

        }
        else{
                http_response_code(503);
                echo json_encode(["message" => "Nothing was add"]);         // If one data send are empty or null return this
            }
        }
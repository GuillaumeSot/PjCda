<?php

    include_once '../../Config/connectionDb.php';

    if(intval($id) == 0){                                   // If id = 0 or empty return this
        http_response_code(503);
        echo json_encode(array("message" => "Please choose a post (ex: 'https://example.php/2')"));  
    }

    else{

        $stmt = $pdo->prepare($sql);    // Verifys in post if object exist 
        $fetchstmt = fetch($stmt);

        if($fetchstmt != false){
        $sql = "DELETE FROM post WHERE post.id = $id";     
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        http_response_code(200);
        echo json_encode(["message" => "Post remove"]);                    // If it exists delete it 
    }
    else{
        http_response_code(503);
        echo json_encode(["message" => "This post doesn't exist or can't be delete"]);          //If it doesn't exists return this
    }
}
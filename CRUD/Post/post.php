<?php
include_once '../../Config/header.php';
header("Access-Control-Allow-Methods: GET, DELETE, PATCH"); //Authorizes GET, DELETE and PATCH
include_once '../../Config/method.php';


$sql = "SELECT p.id,p.post_date,p.content,p.topic_id,topic.title AS topic_title,p.user_id,user.email AS user,category.label AS category_label_topic FROM post AS p
LEFT JOIN topic ON p.topic_id = topic.id
LEFT JOIN user ON p.user_id = user.id 
LEFT JOIN category ON topic.category_id = category.id                                       
WHERE p.id = $id";                                                              // A sql request , recups category by id in url

if($_SERVER['REQUEST_METHOD'] == 'GET'){                    // Does if method is "get"

    include_once '../../Config/connectionDb.php';

    if(intval($id) == 0){                                   // If id = 0 or empty return this
        http_response_code(404);
        echo json_encode(array("message" => "Please choose a post (ex: 'https://example.php/2')"));  
    }

    else{

        $stmt = $pdo->prepare($sql);
        $fetchstmt = fetch($stmt);
        if($fetchstmt != null){                                     // Verifys in category if object exist
            foreach($fetchstmt as $row ){
                
            extract($row);
            if($user != null){                  //Verifys if user exist , if not return "The user has been deleted"
                $post = [
                    "id" => $id,
                    "post_date" => $post_date,
                    "content" => $content,
                    "topic_id" => $topic_id,
                    "topic_title" => $topic_title,
                    "user_id" => $user_id,
                    "user" => $user,
                    "category_label_topic" => $category_label_topic
                ];
                
                $return["results"]["Post"][] = $post;                  //return this in Post array
                }
            else{
                $post = [
                    "id" => $id,
                    "post_date" => $post_date,
                    "content" => $content,
                    "topic_id" => $topic_id,
                    "topic_title" => $topic_title,
                    "user_id" => $user_id,
                    "user" => "The user has been deleted",
                    "category_label_topic" => $category_label_topic
                ];
                
                $return["results"]["Post"][] = $post;        

                }    
            }
            
            http_response_code(200);
            echo json_encode($return);

        }
        else {
            http_response_code(404);
            echo json_encode(array("message" => "Post doesn't exist"));                 // If this post doesn't exist return this
        }
    }
}
elseif($_SERVER['REQUEST_METHOD'] == 'DELETE'){                             // Does if method is "delete"
    include_once('delete.php');     

}
elseif($_SERVER['REQUEST_METHOD'] == 'PATCH'){                          // Does if method is "patch"
    include_once('update_patch.php');

}
else{
    http_response_code(405);
    echo json_encode(["message" => "Method is not allowed"]);           // Return this if no methods work 
}
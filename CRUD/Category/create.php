<?php

    include_once '../../Config/connectionDb.php';

    // $label = secur_data($_POST['label']);            By url


    $request = $pdo->prepare("SELECT * FROM category WHERE label = '".$label."'");
    $stmtfetch = fetch($request);                                                    //Search in category if label with same name exist  

    if($stmtfetch != null){                                                 
        echo json_encode(["message" => "label already exists"]);
        http_response_code(403);
    }
    else{

        if(!empty($label) && isset($label)){                                            // Verifys that data send are not null and not empty

            $stmt = $pdo->prepare("INSERT INTO category (label) VALUES (:lab);");
            $stmt->bindParam(':lab', $label);
            $stmt->execute();

            http_response_code(201);                                                      //Add data at category
            echo json_encode(["message" => "A category has been added"]);

        }
        else{
                http_response_code(503);
                echo json_encode(["message" => "Nothing was add"]);           // If one data send are empty or null return this
            }
        }
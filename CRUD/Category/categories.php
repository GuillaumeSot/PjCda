<?php
include_once '../../Config/header.php';
header("Access-Control-Allow-Methods: GET, POST, PUT"); // authorizes GET, POST and PUT
include_once '../../Config/method.php';

$label = secur_data($data->label);                      // Recups data that we post and secures it
$category_id = secur_data($data->id);

if($_SERVER['REQUEST_METHOD'] == 'GET'){   // Does if method is get


    include_once '../../Config/connectionDb.php';

    $stmt = $pdo->prepare("SELECT * FROM category");
    $fetchstmt = fetch($stmt);                                      // Recups all field from category and puts in associative array

    $return["count"] = count($fetchstmt);                           // Counts how many category object in this array  
    $return["results"]["Categories"] = $fetchstmt;                  // Put in Categories tab

    http_response_code(200);
    echo json_encode($return);                                       // Display in json  

}

elseif($_SERVER['REQUEST_METHOD'] == 'POST'){                  // Does if method is "post"
    include_once('create.php');
}

elseif($_SERVER['REQUEST_METHOD'] == 'PUT'){                  // Does if method is "put"
    include_once('update_put.php');
}

else{
    http_response_code(405);
    echo json_encode(["message" => "Method is not allowed"]);                   // Return this if no methods work 
}
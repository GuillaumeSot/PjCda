<?php

    include_once '../../Config/connectionDb.php';
   


    if(intval($id) == 0){                     // If id = 0 or empty  return this
        http_response_code(503);                                   
        echo json_encode(array("message" => "Please choose a category (ex: 'https://example.php/2')"));   
    }

    else{
        $topic = $pdo->prepare("SELECT topic.id FROM topic WHERE topic.category_id = $id ");
        $fetch_topic = fetch($topic);

        $stmt = $pdo->prepare($sql);
        $fetchstmt = fetch($stmt);

        if($fetchstmt != false){                                 // Verifys in category if object exist 
        $sql = "DELETE FROM category WHERE category.id = $id";
        $sql_topic = "DELETE FROM topic WHERE topic.category_id = $id";
        foreach($fetch_topic as $row ){                     
            foreach($row as $idpost){
                $stmt_post = $pdo->prepare("DELETE FROM post WHERE post.topic_id ='".$idpost."'");      //Delete the post of the topic that will be deleted
                $stmt_post->execute();
    
            }
        }
          
        $stmt_topic = $pdo->prepare($sql_topic);     
        $stmt = $pdo->prepare($sql);                                 
        $stmt_topic->execute();                     // Delete the topic of the category that will be deleted
        $stmt->execute();                        // Delete category                      
        

        http_response_code(200);
        echo json_encode(["message" => "Category remove"]);
    }
    else{
        http_response_code(404);
        echo json_encode(["message" => "This category doesn't exist or can't be delete"]);    //If it doesn't exists return this
    }
}

<?php

    include_once '../../Config/connectionDb.php';

    if(intval($id) == 0){                                   // If id = 0 or empty return this
        http_response_code(503);
        echo json_encode(array("message" => "Please choose a user (ex: 'https://example.php/2')"));  
    }
    else{

        $email = secur_data($data->email);
        $password = secur_data($data->password);            // Recups data that we post and secures it
        $birth_date = secur_data($data->birth_date);

        $stmt = $pdo->prepare($sql);
        $fetchstmt = fetch($stmt);                          // Recups field of user by id and put it on associative array

        $request = $pdo->prepare("SELECT * FROM user WHERE email = '".$email."' EXCEPT SELECT * FROM user WHERE user.id = $id");
        $requestfetch = fetch($request);            // See if email exists on user table except in this current row

        $email = ifempty($email ,$fetchstmt[0]["email"]);               // If we send data in the field, data = new one,else we keep current data
        $password = ifempty($password ,$fetchstmt[0]["password"]);
        $birth_date = ifempty($birth_date ,$fetchstmt[0]["birth_date"]);

        if(count($requestfetch)>=1 && $fetchstmt != null){          // If data exists in other row and row with id url exists , return this 
            echo json_encode(["message" => "Email was taken"]);
            http_response_code(403);
        }
        else{ 

            $password = str_replace(" ", "", $password);            //Takes off space of password

            if(strlen($password)<7){                            //Verifys that password length  count 7
                echo json_encode(["message" => "Password need minimum 7 characters without space"]);            
                http_response_code(403);
            }
            else{
                if ($data->email == null && $data->password == null && $data->birth_date == null){      //Verifys that at least one field has received data, if no field has received data return this
                    echo json_encode(["message" => "Nothing was modified"]);
                    http_response_code(503);
                }

                else{
                    if(!empty($email) && isset($email) && !empty($password) && isset($password) && !empty($birth_date) && isset($birth_date) && $fetchstmt != null){    // Verifys that data send are not null and not empty
                        $sql = "UPDATE user SET email = :mail,password = :mdp ,birth_date = :date WHERE id = $id";

                        $stmt = $pdo->prepare($sql);
                        $stmt->bindParam(':mail', $email);
                        $stmt->bindParam(':mdp', $password);
                        $stmt->bindParam(':date', $birth_date);                                 //Update this
                        $stmt->execute();

                        http_response_code(200);
                        echo json_encode(["message" => "A user has been modified"]);
                }
                    else{
                        http_response_code(503);
                        echo json_encode(["message" => "modification does not work"]);          // If one data are null or empty return this 
                }
            }
        }
    }
}

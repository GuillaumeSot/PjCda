<?php
include_once '../../Config/header.php';
include_once '../../Config/method.php';
header("Access-Control-Allow-Methods: GET, DELETE, PATCH"); //Authorizes GET, DELETE and PATCH

$sql= "SELECT u.id,u.email,u.password,u.birth_date FROM user AS u 
WHERE u.id = $id";                                      // A sql request , recups user by id in url

if($_SERVER['REQUEST_METHOD'] == 'GET'){                // Does if method is "get"

    include_once '../../Config/connectionDb.php';

    if(intval($id) == 0){                                   // If id = 0 or empty return this
        http_response_code(404);
        echo json_encode(array("message" => "Please choose a user (ex: 'https://example.php/2')"));  
    }
    else{

        $stmt = $pdo->prepare($sql);
        $fetchstmt = fetch($stmt);
        if($fetchstmt != null){         // Verifys in user if object exist

            foreach($fetchstmt as $row ){
            extract($row);
            
                $user = [
                    "id" => $id,
                    "email" => $email,
                    "password" => $password,
                    "birth_date" => $birth_date,
                ];
                
                $return["result"]["User"][] = $user; //return this in User array
            }
            echo json_encode($return);
        }

        else{
            http_response_code(404);
        
            echo json_encode(array("message" => "User doesn't exist"));             // If this user doesn't exist return this
        }
    }
}

elseif($_SERVER['REQUEST_METHOD'] == 'DELETE'){                         // Does if method is "delete"
    include_once('delete.php');

}
elseif($_SERVER['REQUEST_METHOD'] == 'PATCH'){                          // Does if method is "patch"
    include_once('update_patch.php');

}

else{
    http_response_code(405);
    echo json_encode(["message" => "Method is not allowed"]);       // Return this if no methods work 
}
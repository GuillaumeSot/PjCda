<?php

    include_once '../../Config/connectionDb.php';

    if(intval($id) == 0){                                   // If id = 0 or empty return this
        http_response_code(503);
        echo json_encode(array("message" => "Please choose a user (ex: 'https://example.php/2')"));  
    }
    else{

    $stmt = $pdo->prepare($sql);
    $fetchstmt = fetch($stmt);

    if($fetchstmt != false){                                // Verifys in user if object exist 

        $sql = "DELETE FROM user WHERE user.id = $id";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();                                       // If it exists delete it 

        http_response_code(200);
        echo json_encode(["message" => "User remove"]);
    }

    else{
        http_response_code(503);
        echo json_encode(["message" => "This user doesn't exist or can't be delete"]);          //If it doesn't exists return this  
    }
}


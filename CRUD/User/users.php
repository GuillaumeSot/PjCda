<?php
include_once '../../Config/header.php';
header("Access-Control-Allow-Methods: GET, POST, PUT");  // authorizes GET, POST and PUT
include_once '../../Config/method.php';

$email = secur_data($data->email);
$password = secur_data($data->password);              // Recups data that we post and secures it
$birth_date = secur_data($data->birth_date);
$user_id = secur_data($data->id);

if($_SERVER['REQUEST_METHOD'] == 'GET'){                    // Does if method is get

    include_once '../../Config/connectionDb.php';
    include_once '../../Config/method.php';

    $stmt = $pdo->prepare("SELECT * FROM user ");               // Recups all field from user and puts in associative array
    $fetchstmt = fetch($stmt);

    $return["count"] = count($fetchstmt);                   // Counts how many user object in this array
    $return["results"]["Users"] = $fetchstmt;                // Puts in Users tab

    http_response_code(200);
    echo json_encode($return);                                  // Displays in json 

}

elseif($_SERVER['REQUEST_METHOD'] == 'POST'){                   // Does if method is "post"
    include_once('create.php');
}

elseif($_SERVER['REQUEST_METHOD'] == 'PUT'){                    // Does if method is "put"
    include_once('update_put.php');
}

else{
    http_response_code(405);
    echo json_encode(["message" => "Method is not allowed"]);           // Return this if no methods work 
}
# Projet CDA

Au début de ce projet j'ai été voir toutes les normes d'une API REST. Par la suite je me suis mis à la création des entitées. Je les ai faites directement dans ma base de données et pour se faire j'ai utilisé sqlite. Une fois que toutes celles-ci aient été créées j'ai commencé à préparer mes dossiers dans postman pour les entitées User,Category,Topic et Post et c'est à ce moment que j'ai débuté le développement de l'api en php. Pour me connecter à la base de données depuis mes fichiers php j'ai utilisé PDO. Pour tester si mon api fonctionne j'ai beaucoup utilisé postman en mettant tous les points d'entrées de l'api pour chaque entitée créée. J'ai également  grâce à postman pu essayer de mettre un message d'erreur et une reponse pour toutes les erreurs qu'il serait possible de rencontrer en utilisant l'api.

J'ai assigné à "users.php", "categories.php", "topics.php" et "posts.php" les méthodes Get, Post et Put. Ils comportent en eux les fichiers "create.php" et "update_put.php"
J'ai assigné à "user.php", "category.php", "topic.php" et "post.php" les méthodes Get(par l'id),Delete et Patch. Ils comportent en eux les fichiers "delete.php" et "patch.php". Pour accéder à leurs points d'entrées il faudra mettre l'id de la ligne avec laquelle on veut intéragir dans l'url comme ceci :
"https://example.php/6".

Une fois que tous les points d'entrées de l'api ont été créer et fonctionnent j'ai pu commencé à ajouter des fonctionnalités.

-La suppression en cascade des lignes topics liées à une catégorie ainsi que la suppression des postes liées à ces topics qui viennent d'être supprimé.
-La suppression des postes liées au topics lors de leurs suppressions.
-Lorsque un utilisateur liée à un topic ou un post sera supprimé , il sera marqué lors de l'affichage du post ou topic, que le profil de cette utilisateur à été supprimé.
-Lors de la création d'un poste il y aura une vérification que la catégorie liée à celui ci éxiste bel et bien et il en va de même pour la création de poste où l'on vérifiera que le topic lié à lui éxiste.
-On ne pourra pas créer deux fois la même catégorie et pendant la création d'un utilisateur celui ci ne pourra pas avoir une adresse email déjà éxistante.
-Pour les mots de passes, tous les espaces seront supprimés et il devra faire au minimum 7 caractères pour être valide. 

Pour ce projet j'ai principalement utilisé php, postman et sqlite.

Les plus grosses difficultés rencontré durant ce projet ont été beaucoup d'erreurs d'étourdie qui m'ont fait perdre beaucoup de temps, un caractère en trop ou en moins, une virgule de trop dans un json, un mauvais point d'entré dans postman. Lorsque je rencontrais des erreurs, j'avais très souvent un message dans ma console qui me disait où était la ligne de mon erreur et puisque j'ai fait mon possible pour essayé de couvrir tous les problèmes pouvant être rencontré lors de l'utilisation de l'api avec le bon message , au bout d'un moment je pouvais voir très facilement où étaient mes erreurs. Je faisais également quelque var_dump() pour connaître ce que contenait mes variables et arrangés les choses lorsqu'un problème était rencontré. 
J'ai eu un peu de mal pour ce qui est du fait de ne pas trop répéter mon code.

Je ne sais pas exactement en combien de temps je l'ai fais, car je travaillais sur le projet un petit temps le soir après mon stage, mais environ 4 ou 5 jours je pense. 



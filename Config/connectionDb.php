<?php
try{
    $pdo = new PDO("sqlite:../../cda.db");
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);    //connection to database
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
} catch(Exception $e) {
    echo "Connexion error".$e->getMessage();
    die();

}

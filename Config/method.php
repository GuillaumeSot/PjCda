<?php
function fetch($a){
    $a->execute();
    $b = $a->fetchAll(PDO::FETCH_ASSOC);    // Function for execute pdo and return associative array of request
    return $b;
}
function secur_data($secur){
    $secur = trim($secur);
    $secur = stripslashes($secur);             // Function for secur the data
    $secur = strip_tags($secur);
    $secur = htmlspecialchars($secur);
    return $secur;
}
function ifempty($a , $b){
    if ($a == "" ){                                     // Verify if a variable is empty, if it is  , it will return another variable if not return the 1st variable
        $a = $b;
        return $a;
        
    }
    else{
        return $a;
    }
}
$data = json_decode(file_get_contents("php://input"));                  // Recup json data that we post
// $id = secur_data($_GET['id']);                     // Recup id in url

// if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
//     $url = "https"; 
//   else
//     $url = "http"; 
    
//   // Ajoutez // à l'URL.
//   $url .= "://"; 
    
//   // Ajoutez l'hôte (nom de domaine, ip) à l'URL.
//   $url .= $_SERVER['HTTP_HOST']; 
    
//   // Ajouter l'emplacement de la ressource demandée à l'URL
//   $url .= $_SERVER['REQUEST_URI']; 
      
//   // Afficher l'URL
//   echo $url; 
$url = $_SERVER[REQUEST_URI];
$url = explode("/", $url);
$id = $url[count($url) - 1];
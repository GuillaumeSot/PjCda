<?php
header("Access-Control-Allow-Origin: *");            // Authorization api acces  , here it's public
header("Content-Type: application/json; charset=UTF-8");           // Response in json , standard utf8 
header("Access-Control-Max-Age: 3600");                             // Max time of request
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"); 
// Header authorized

// Header for standard api rest 